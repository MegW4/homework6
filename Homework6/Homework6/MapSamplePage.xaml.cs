﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace Homework6
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MapSamplePage : ContentPage
	{
		public MapSamplePage ()
		{
			InitializeComponent ();
            var initialMapLocation = MapSpan.FromCenterAndRadius
                                            (new Position(33.1307785, -117.1601826), Distance.FromMiles(1));

            MySampleMap.MoveToRegion(initialMapLocation);


            PlaceAMarker();

            var street = new Button { Text = "Street" };
            var hybrid = new Button { Text = "Hybrid" };
            var satellite = new Button { Text = "Satellite" };
            street.Clicked += HandleClicked;
            hybrid.Clicked += HandleClicked;
            satellite.Clicked += HandleClicked;
            var segments = new StackLayout
            {
                Spacing = 30,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                Orientation = StackOrientation.Horizontal,
                Children = { street, hybrid, satellite }
            };
        }

        public MapSamplePage(object sender, EventArgs e)
        {

        }

        private void PlaceAMarker()
        {
            var position = new Position(33.1307785, -117.1601826); // Latitude, Longitude
            var pin = new Pin
            {
                Type = PinType.Place,
                Position = position,
                Label = "custom pin",
                Address = "custom detail info"
            };
            var pin3 = new Pin
            {
                Type = PinType.Place,
                Position = new Position(33, -100),
                Label = "Come visit",
                Address = "We have everything"
            };
            var pin4 = new Pin
            {
                Type = PinType.Place,
                Position = new Position(40, -30),
                Label = "Come here!",
                Address = "We have the best prices"
            };
            var pin5 = new Pin
            {
                Type = PinType.Place,
                Position = new Position(90, -300),
                Label = "Gotta visit",
                Address = "We'll make you want to stay."
            };
            var pin6 = new Pin
            {
                Type = PinType.Place,
                Position = new Position(50, -500),
                Label = "Don't leave",
                Address = "We have everything you need"
            };


            MySampleMap.Pins.Add(pin);
        }

        void HandleClicked(object sender, EventArgs e)
        {
            var b = sender as Button;
            switch (b.Text)
            {
                case "Street":
                    MySampleMap.MapType = MapType.Street;
                    break;
                case "Hybrid":
                    MySampleMap.MapType = MapType.Hybrid;
                    break;
                case "Satellite":
                    MySampleMap.MapType = MapType.Satellite;
                    break;
            }
        }
    }
}