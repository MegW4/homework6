﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Homework6
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
            PopulatePicker();
        }

        private void PopulatePicker()
        {
            Dictionary<string, Color> nameToColor = new Dictionary<string, Color>()
            {
                { "Walmart", Color.Aqua },
                { "Target", Color.Gray },
                { "PetCo", Color.Lime },
                { "Ralphs", Color.Navy },
                { "", Color.Purple },
                { "Silver", Color.Silver },
                { "White", Color.White },
            };


            foreach (var item in nameToColor)
            {
                SamplePicker.Items.Add(item.Key);
            }

            SamplePicker.SelectedIndex = 0;
        }
        void Handle_NavigateToMapSample(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new MapSamplePage());
        }

        void Handle_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new MapSamplePage(sender, e));// Code to handle user making index changes in picker
        }
    }
}
